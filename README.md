# UniBoAT - Energy Management Controller and Route Assistant

![](./pics/logo.png)

## 2021

This is a full and real-time controller to manage powertrains based on different energy sources. It is based on optimal control theory and it has been derived from Dynamic Programming algorithms, modified and implemented as an ECMS - Equivalent Consumption Minimization Strategy - to be fully compatible with real time applications. The objective of the controller is to optimally manage a powertrain with different energy sources (hydrogen and Li-ion battery as an example) to maximize the driving or cruising range by keeping each energy conversion subsytem at operating points as close as possible to the maximum efficiency ones. The open source package, developed in LabView 2018, includes a friendly GUI (Graphical User Interface) so that it can be used without writing new code but simply inserting the main parameters of the specific powertrain.

### Front Panel

![](./pics/front_panel.jpg)

### Block Diagram

![](./pics/block_diagram.jpg)

## 2022

This is a route assistance algorithm based on a path finding concept. It takes the current position from the GPS, the previous position and the desired target position. The objective of the algorithm is to suggest the direction (azimuth direction) of the shortest route in order to reach a fixed target. All of this while trying to take into consideration also some external mismatch due to, for example, wind or sea current. Hence, it elaborates an error which increases along with the distance from the target. In addition, since the GPS may oscillate or not be very accurate, the algorithm is robust to sudden variations of the GPS itself.  The open source package, developed in LabView 2018, includes a GUI (Graphical User Interface) so that it can be used without writing new code but simply by inserting the main parameters of the specific powertrain. 

### Block Diagram
![](./pics/Block_diagram_2022.png)

# UniBoAT - ADAS

## 2023

This dataset consists of 7468 stereo-frames at resolution of 2048x1536 pixels each. The dataset was captured over a period of approximately 15 days in port of Marina di Ravenna, using our acquisition system. To maximize the visual diversity of the environment, the dataset was recorded at different times of day and under various weather conditions. We report two sets of data acquisition. All sequences are time-synchronized and and images are rectified.

These sets of images have allowed the study and optimization of a system made up of two cameras which allows to recognize objects and calculate depth.